CC=g++

LD_SPDK_LIBS= -lspdk  -lspdk_bdev_malloc -lspdk_blob  -lspdk_event -lspdk_event_vhost  -lspdk_log_rpc  -lspdk_scsi \
-lspdk_virtio -lspdk_app_rpc -lspdk_bdev_null  -lspdk_blob_bdev -lspdk_event_bdev -lspdk_event_vmd \
-lspdk_lvol -lspdk_sock -lspdk_vmd -lspdk_bdev -lspdk_bdev_nvme  -lspdk_blobfs -lspdk_event_copy \
-lspdk_ftl  -lspdk_nbd  -lspdk_sock_posix -lspdk_bdev_aio -lspdk_bdev_passthru  -lspdk_blobfs_bdev \
-lspdk_event_iscsi  -lspdk_ioat  -lspdk_net  -lspdk_thread -lspdk_bdev_delay  -lspdk_bdev_raid \
-lspdk_conf  -lspdk_event_nbd -lspdk_iscsi -lspdk_notify -lspdk_trace -lspdk_bdev_error  -lspdk_bdev_rpc \
-lspdk_copy  -lspdk_event_net -lspdk_json  -lspdk_nvme  -lspdk_bdev_gpt -lspdk_bdev_split \
-lspdk_copy_ioat -lspdk_event_nvmf -lspdk_jsonrpc  -lspdk_nvmf -lspdk_util -lspdk_bdev_lvol \
-lspdk_bdev_virtio -lspdk_env_dpdk -lspdk_event_scsi -lspdk_log  -lspdk_rpc  -lspdk_vhost 

DPDK_LIBS= -ldpdk -lrte_bus_vdev -lrte_compressdev -lrte_eal -lrte_hash   -lrte_mbuf -lrte_mempool_bucket -lrte_meter -lrte_pci  -lrte_vhost\
-lrte_bus_pci -lrte_cmdline  -lrte_cryptodev -lrte_ethdev -lrte_kvargs -lrte_mempool -lrte_mempool_ring   -lrte_net   -lrte_ring

LDFLAGS= ${LD_SPDK_LIBS} ${DPDK_LIBS}
CFLAGS=-std=c++11 -g -Wall -lpthread

SUBSRCS=$(wildcard ./*.c)
OBJECTS=$(SUBSRCS:.c=.o)

all: fs_test


fs_test: $(OBJECTS) 
	$(CC) $(CFLAGS) $^ $(LDFLAGS) -o $@

clean:
	rm -rf fs_test *.o


