#pragma once

#include "cstdio"

#include "spdk/stdinc.h"

#include "spdk/nvme.h"
#include "spdk/vmd.h"
#include "spdk/env.h"

#include "vector"
#include "thread"
#include "sys/syscall.h"
#include "functional"

namespace rocksdb {

//spdk
struct ns_entry { // namespace
	struct spdk_nvme_ctrlr	*ctrlr;
	struct spdk_nvme_ns	*ns;
	struct ns_entry		*next;
	struct spdk_nvme_qpair	**qpair;
	uint32_t sector_size;
};

struct spdk_info{
	struct spdk_nvme_ctrlr *controller = nullptr;
	int num_ns;  // number of namespace
	struct ns_entry *namespaces = nullptr;
	char name[200];
	std::string addr;
};

static void cleanup(struct spdk_info *spdk_info){
	struct ns_entry *ns_entry = spdk_info->namespaces;
	while (ns_entry) {
		struct ns_entry *next = ns_entry->next;
		free(ns_entry);
		ns_entry = next;
	}

	struct spdk_nvme_ctrlr *ctrlr = spdk_info->controller;
	if (ctrlr) {		
		free(ctrlr);
	}
}

struct spdk_data {
	struct ns_entry	*ns_entry;
	char *buf;
	bool is_completed;
};

static void register_ns(struct spdk_info *spdk_info, struct spdk_nvme_ctrlr *ctrlr, struct spdk_nvme_ns *ns){
	if (!spdk_nvme_ns_is_active(ns)) {
		return;
	}

	struct ns_entry *entry;
	entry = (struct ns_entry *)malloc(sizeof(struct ns_entry));
	if (entry == NULL) {
		perror("ns_entry malloc");
		exit(1);
	}
	entry->sector_size = spdk_nvme_ns_get_sector_size(ns);
	entry->ctrlr = ctrlr;
	entry->ns = ns;
	entry->next = spdk_info->namespaces;
	spdk_info->namespaces = entry;

	printf("Namespace ID: %d size: %juGB\n", spdk_nvme_ns_get_id(ns),spdk_nvme_ns_get_size(ns) / 1000000000);
}

static bool probe_cb(void *cb_ctx, const struct spdk_nvme_transport_id *trid,
		struct spdk_nvme_ctrlr_opts *opts){
	printf("Attaching to %s\n", trid->traddr);
	return true;
}

static void attach_cb(void *cb_ctx, const struct spdk_nvme_transport_id *trid,
	  				struct spdk_nvme_ctrlr *ctrlr, const struct spdk_nvme_ctrlr_opts *opts){
	printf("Attached to %s\n", trid->traddr);

	struct spdk_info *spdk_info = (struct spdk_info *)cb_ctx;

	spdk_info->controller = ctrlr;

	const struct spdk_nvme_ctrlr_data *cdata;
	cdata = spdk_nvme_ctrlr_get_data(ctrlr);
	snprintf(spdk_info->name, sizeof(spdk_info->name), "%-20.20s (%-20.20s)", cdata->mn, cdata->sn);

	int num_ns = spdk_nvme_ctrlr_get_num_ns(ctrlr);
	spdk_info->num_ns = num_ns;

	printf("Using controller %s with %d namespaces.\n", spdk_info->name, num_ns);
	struct spdk_nvme_ns *ns;
	for (int nsid = 1; nsid <= num_ns; nsid++) {
		ns = spdk_nvme_ctrlr_get_ns(ctrlr, nsid);
		if (ns == NULL) {
			continue;
		}
		register_ns(spdk_info, ctrlr, ns);
	}
}

static void init_qpair(struct spdk_info *spdk_info, int num){
	struct ns_entry *ns_entry = spdk_info->namespaces;
	while (ns_entry) {
		ns_entry->qpair = (spdk_nvme_qpair **)calloc(num, sizeof(struct spdk_nvme_qpair *));
		for (int i = 0; i < num; ++i){
			ns_entry->qpair[i] = spdk_nvme_ctrlr_alloc_io_qpair(ns_entry->ctrlr, NULL, 0);
			if (ns_entry->qpair[i] == NULL) {
				printf("ERROR: spdk_nvme_ctrlr_alloc_io_qpair() failed\n");
				return;
			}
		}
		ns_entry = ns_entry->next;
	}
}

static void request_complete(void *arg, const struct spdk_nvme_cpl *completion){
	struct spdk_data *req = (struct spdk_data *) arg;
	if (spdk_nvme_cpl_is_error(completion)) {
		fprintf(stderr, "I/O error status: %s\n", spdk_nvme_cpl_get_status_string(&completion->status));
		fprintf(stderr, "Write I/O failed, aborting run\n");
		req->is_completed = true;
		exit(1);
	}
	req->is_completed = true;
}


static void read_data(struct ns_entry *ns_entry, uint64_t lba, struct spdk_data *req, int req_size, int i){
	assert(req != NULL);
	assert(req->buf != NULL);

	req->is_completed = false;
	req->ns_entry = ns_entry;

	int sector_num = req_size / ns_entry->sector_size;

	int rc = spdk_nvme_ns_cmd_read(ns_entry->ns, ns_entry->qpair[i], req->buf,
		 			    lba*sector_num, /* LBA start */
		 			    sector_num, /* number of LBAs */
					    request_complete, req, 0);

	if (rc != 0) {
		fprintf(stderr, "starting write I/O failed\n");
		exit(1);
	}
	while (!req->is_completed) {
		spdk_nvme_qpair_process_completions(ns_entry->qpair[i], 0);
	}

}

static void write_data(struct ns_entry *ns_entry, uint64_t lba, struct spdk_data *req, int req_size, int i){
	assert(req != NULL);
	assert(req->buf != NULL);

	req->is_completed = false;
	req->ns_entry = ns_entry;

	int sector_num = req_size / ns_entry->sector_size;

	int rc = spdk_nvme_ns_cmd_write(ns_entry->ns, ns_entry->qpair[i], req->buf,
					    lba*sector_num, /* LBA start */
					    sector_num, /* number of LBAs */
					    request_complete, req, 0);

	if (rc != 0) {
		fprintf(stderr, "starting write I/O failed\n");
		exit(1);
	}
	while (!req->is_completed) {
		spdk_nvme_qpair_process_completions(ns_entry->qpair[i], 0);
	}
}


static void write_thread(int tid, struct ns_entry *ns_entry, int num){
	//uint64_t start = spdk_get_ticks();
	struct spdk_data req[num];
	int req_size = 4096;
	for(int i=0;i<num;i++)
	{	
		req[i].buf = (char*)spdk_zmalloc(0x1000, 0x1000, NULL, SPDK_ENV_SOCKET_ID_ANY, SPDK_MALLOC_DMA); // 4KB
		snprintf(req[i].buf, 0x1000, "%s%d", "hello my file ",i);
	}
	int i;
	for(i=0; i < num; i++){
		write_data(ns_entry, 1+i, &req[i], req_size, tid);
	}	

}



static void read_thread(int tid, struct ns_entry *ns_entry, int num){
	//uint64_t start = spdk_get_ticks();
	struct spdk_data req[num];
	int req_size = 4096;
	for(int i=0;i<num;i++)
		req[i].buf = (char*)spdk_zmalloc(0x1000, 0x1000, NULL, SPDK_ENV_SOCKET_ID_ANY, SPDK_MALLOC_DMA); // 4KB

	for(int i=0; i < num; i++){
		read_data(ns_entry, 1+i, &req[i], req_size, tid);
	}

	for(int i=0;i<num;i++)
		printf("id: %d, %s\n",i,req[i].buf);

}

static void test(struct spdk_info *spdk_info){
	struct ns_entry *ns_entry = spdk_info->namespaces;
	//uint64_t start = spdk_get_ticks();
	int num = 8;
	int threads_num = 1;
	std::vector<std::thread> threads;
	std::vector<std::thread> readth;
	for(int i=0; i < threads_num; i++){
		threads.push_back(std::thread(write_thread, i, ns_entry, num));
	}
	for(int i=0; i < threads_num; i++){
		threads[i].join();
	}

	for(int i=0; i < threads_num; i++){
		readth.push_back(std::thread(read_thread, i, ns_entry, num));
	}
	for(int i=0; i < threads_num; i++){
		readth[i].join();
	}


}

static void try_test(struct spdk_info *spdk_info)
{
	struct ns_entry *ns_entry = spdk_info->namespaces;
	int req_size = 4096,resp_size=4096;
	struct spdk_data req,resp;
	req.buf = (char*)spdk_zmalloc(0x1000, 0x1000, NULL, SPDK_ENV_SOCKET_ID_ANY, SPDK_MALLOC_DMA);
	resp.buf = (char*)spdk_zmalloc(0x1000, 0x1000, NULL, SPDK_ENV_SOCKET_ID_ANY, SPDK_MALLOC_DMA);
	snprintf(req.buf, 0x1000, "%s", "Hello world!\n");
	write_data(ns_entry, 0, &req, req_size, 0);
	printf("write complete\n");
	read_data(ns_entry, 0, &resp, resp_size, 0);
	printf("%s\n",resp.buf);
}

static struct spdk_info * InitSPDK(std::string device_addr, int qpair_num){
	struct spdk_info *spdk_info = new struct spdk_info;// (struct spdk_info * ) malloc(sizeof(struct spdk_info));
	if(spdk_info == nullptr){
		fprintf(stderr, "Unable to malloc space for spdk info\n");
		return nullptr;
	}
	spdk_info->addr = device_addr;

	struct spdk_env_opts opts;
	spdk_env_opts_init(&opts);
	opts.shm_id = 0;  //shared memory group ID
	if (spdk_env_init(&opts) < 0) {
		fprintf(stderr, "Unable to initialize SPDK env\n");
		return nullptr;
	}

	struct spdk_nvme_transport_id trid;
	if(spdk_nvme_transport_id_parse(&trid, device_addr.c_str()) != 0){
		fprintf(stderr, "spdk_nvme_transport_id_parse() failed\n");
		cleanup(spdk_info);
		return nullptr;
	}

	if(spdk_nvme_probe(&trid, spdk_info, probe_cb , attach_cb , NULL) != 0){
		fprintf(stderr, "spdk_nvme_probe() failed\n");
		cleanup(spdk_info);
		return nullptr;
	}

	if (spdk_info->controller == NULL) {
		fprintf(stderr, "no NVMe controllers found\n");
		cleanup(spdk_info);
		return nullptr;
	}

	init_qpair(spdk_info, qpair_num);
	test(spdk_info);
	return spdk_info;
}

}
